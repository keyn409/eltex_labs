#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <time.h>

#define buff_size 256 //размер буфера
#define max_len 7 //максимальная длина рандомной строки
#define max_sleep 4 //максимальное время засыпания

//ошибки
void error(char* str){
    perror(str);
    exit(1);
}

int UDPSocket_creating(int UDP_port) {
    int UDP_sock;
    struct sockaddr_in client_addr; //адрес источника сообщений извне
    //Сoздание сокета UDP
    if ((UDP_sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        error("socket() failed");

    //Заполнение полей структуры UDP клиента
    memset(&client_addr, 0, sizeof(client_addr)); //зануляем структуру
    client_addr.sin_family = AF_INET;
    client_addr.sin_addr.s_addr = htonl(INADDR_ANY); //IP-адрес сервера
    client_addr.sin_port = htons(UDP_port);//Порт
    //client_addr.sin_port = htons(IPPORT_USERRESERVED);//Порт

    //делаем bind адреса
    int a = 1;
    while (1)
    {
        if (bind(UDP_sock, (struct sockaddr*) &client_addr, sizeof(client_addr)) < 0) {
            a++;
            client_addr.sin_port = htons(UDP_port + a);//Порт
            continue;
        }
        printf("Адрес получил!\n");
        break;
    }
    return UDP_sock;
}

int main(int argc, char** argv){
    if (argc < 4)
        error("Usage: ./client Server_IP TCP_Port UDP_Port");
    int TCP_sock; //TCP-сокет клиента
    int UDP_sock; //UDP-сокет клиента
    char* server_IP = argv[1]; //IP-адрес сервера
    unsigned short TCP_port = atoi(argv[2]); // TCP-порт
    unsigned short UDP_port = atoi(argv[3]); // UDP-порт
    server_IP = argv[1];

    char buff[buff_size];
    struct sockaddr_in serv_addr; ///адрес сервера
    unsigned int msg_len; //размер данных, полученных от сервера

    //Заполняем TCP структуру serv_addr
    memset(&serv_addr, 0, sizeof(serv_addr)); //зануляем структуру
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(server_IP); //IP-адрес сервера
    serv_addr.sin_port = htons(TCP_port);//Порт

    UDP_sock = UDPSocket_creating(UDP_port);

    srand(time(NULL));
    while (1) {
        //Получаем от сервера broadcast-сообщения
        memset(&buff, 0, sizeof(buff)); //зануляем буффер
        if ((msg_len = recvfrom(UDP_sock, buff, buff_size, 0, NULL, 0)) < 0)
            error("recvfrom() failed");

        //указываем конец сроки
        buff[msg_len] = '\0';
        printf("Получил сообщение: %s\n", buff);

        if (strcmp(buff, "Жду сообщений!" ) == 0){
            //Cоздание TCP сокета
            if ((TCP_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
                error("socket");
            memset(&buff, 0, sizeof(buff)); //зануляем буффер
            buff[0] = '1'; //задаём тип клиента
            printf("Отправляем серверу свой тип!\n");
            //коннектимся по TCP
            if (connect(TCP_sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
                error("connect");
            //Отправляем серверу тип клиента
            if (send(TCP_sock, buff,buff_size, 0) != buff_size)
                error("send");
            //Готовим строку для отправки
            memset(buff, 0, sizeof(buff)); //зануляем буффер
            int range = 1 + rand() % max_len;
            for (int i = 0; i < range; i++){
                buff[i] = 'A' + rand()%58;
            }
            printf("Buff - %s\n", buff );

            //Отправляем серверу строку по TCP
            if (send(TCP_sock, buff,buff_size, 0) != buff_size)
                error("send");
            //Закрываем сокет
            close(TCP_sock);
            //После отправки клиент засыпает
            int tmp = 1 + rand() % max_sleep;
            printf("Иду спать на %d секунд;\n", tmp );
            sleep( tmp );
        }
    }

    return 0;

}
