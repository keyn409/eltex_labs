#!/bin/bash

if [ -n "$1" ]
then
index=0;
else
echo "Usage: ./script.sh count_client1 count_client2 "
exit 1
fi

if [ -n "$2" ]
then
tmp=0;
else
echo "Usage: ./script.sh count_client1 count_client2 "
exit 1
fi

xterm -geometry 80x27+1930+626 -e ./server 192.168.0.255 3000 3001  &

while [[ $index -ne $1 ]]; do
    xterm -e ./client_1 127.0.0.1 3000 3001 &
    let "index = index + 1";
done



while [[ $tmp -ne $2 ]]; do
    xterm -e ./client_2 127.0.0.1 3000 3001 &
    let "tmp = tmp + 1";
done
