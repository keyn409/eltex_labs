#include <string.h>     /* for memset() */
#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), bind(), and connect() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <sys/msg.h>    /* for queue */
#include <sys/ipc.h>    /* for key_t*/
#include <sys/shm.h>    /* for shared memory*/
#include <sys/sem.h>    /* for shared memory*/

#define max_connect 10 //максмальное количество одновременных подключений по TCP
#define buff_size 256 //размер буфера
#define msg_max 10 //максимальное количество сообщений в очереди
#define sleep_client1 3 //интервал отправки пакетов "Жду сообщений"
#define sleep_client2 4 //интервал отправки пакетов "Есть сообщения"

union semun { //объединение, необходимое для семаформа
    int val; //значение для SETVAL
    struct semid_ds *buf; //буферы для IPC_STAT и IPC_SEt
    unsigned short *array; //массивы для GATALL и SETALL
    struct seminfo *__buf; //буфер для IPC_INFO
};
//структуры для семафора
struct sembuf lock_res = {0, -1, 0}; //блокировка ресурса
struct sembuf res_res = {0, 1, 0}; //освобождение ресурса

//для разделяемой памяти
int *shm; //в разделяемой памяти будет храниться количество сообщений в очереди
int shmid;
//для семафора
int semid;
union semun arg;
key_t key; //ключ для инициализации очереди

//структура для очереди
struct QUEUE
{
    long mtype;
    char str[buff_size];
};

typedef struct QUEUE queue;

//ошибки
void error(char* msg) {
    perror(msg);
    exit(1);
}

int shm_work(int tmp) {
    int result;//переменная для возвращения результата
    //Получение доступа к разделяемой памяти
    if ((shm = (int*)shmat(shmid, NULL, 0)) == (int*) -1)
        error("shmat() failed");

    //Заблокируем разделяемую память
    if ((semop(semid, &lock_res, 1)) == -1)
        error("Semop() - Lock failed\n");

    if (tmp == 0)
        *(shm) = tmp;
    else if (tmp == 1 || tmp == -1)
        *(shm) = *(shm) + tmp;
    result = *(shm);

    //Освободим разделяемую память
    if ((semop(semid, &res_res, 1)) == -1)
        error("Semop() - Unlock failed\n");

    //Отключаемся от разделяемой памяти
    if (shmdt(shm) < 0)
        error ("shmdt() failed");
    return result;
}

//Создание сокета для TCP-сервера и привязывание адреса
int TCPSocket_creating (unsigned short port) {
    int sock; //сокет для сервера
    struct sockaddr_in serv_addr;

    //Создание сокета
    if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        error("socket() failed");

    //Заполнение полей структура
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY); //IP-адрес
    serv_addr.sin_port = htons(port); //Порт

    //Привязывание сокета к локальному адресу
    if (bind(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        error("bind() failed");
    //Выствляем сокет на прослушку входящиъ соединений
    if (listen(sock, max_connect) < 0)
        error("listen() failed");
    printf(" TCP-сокет готов! Порт - %d\n", port );
    return sock;
}

//Создание UDP-сокета
int UDPSocket_creating(){
    int UDP_sock;
    //Создание сокета UDP
    if ((UDP_sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        error("socket() failed");
    //Устанавливаем флаг для сокета
    int broadcast = 1;
    if (setsockopt(UDP_sock, SOL_SOCKET, SO_BROADCAST, (void *) &broadcast, sizeof(broadcast)) < 0)
        error("setsocket() failed");
    return UDP_sock;
}

//Подключение клиента для TCP-сокета
int Accept_TCPConnection(int serv_cock) {
    int client_sock; //Сокет для клиента
    struct sockaddr_in client_addr; //АДрес клиента
    unsigned int client_len; //Размер структуры адреса клиента

    //Определяем размер структуры клиента
    client_len = sizeof(client_addr);

    //Ожидаем подключения от клиента
    if ((client_sock = accept(serv_cock, (struct sockaddr *) &client_addr, &client_len)) < 0)
        error("Accept() failed");
    return client_sock;
}

void client_type_one(int client_sock, int msgqid ) {
    char buff[buff_size];//буффер
    int msg_len; //размер сообщения
    queue message; //для отправления сообщения в очередь
    int count_msg; //количество сообщений в очереди
    //Принимаем сообщение от клиента
    if ((msg_len = recv(client_sock, buff, buff_size, 0)) < 0)
        error("recv() failed");
    buff[strlen(buff)] = '\0'; //указываем конец строки
    printf("Клиент прислал сообщение - %s, размер - %d\n", buff, msg_len );
    //Записываем полученное сообщение в очередь
    strcpy(message.str, buff);
    message.mtype = 1;
    if ((msgsnd(msgqid,&message, sizeof(message) - sizeof(long), 0)) == -1)
        error("msgsnd() failed");
    count_msg = shm_work(1); //увеличиваем количество сообщений в очереди
    printf("Количество сообщений в очереди - %d\n", count_msg );
    close(client_sock);
}
void client_type_two(int client_sock, int msgqid ){
    char buff[buff_size];//буффер
    int rc; //для считывания сообщения из очереди
    queue message; //для считывания сообщения из очереди
    int count_msg; //количество сообщений в очереди
    //Считываем сообщение из очереди
    memset (buff, 0, sizeof(buff)); //занулим буффер
    if ((rc = msgrcv(msgqid, &message, sizeof(message) - sizeof(long), 0, 0)) < 0 )
        error("msgrcv() failed");
    strcpy(buff, message.str);
    //Отправляем сообщение клиенту
    if (send(client_sock, buff, buff_size, 0) != buff_size)
        error("send() failed");
    count_msg = shm_work(-1); //уменьшаем количество сообщений в очереди
    printf("Количество сообщений в очереди - %d\n", count_msg );
    close(client_sock);
}

void broad_wait_msg(int UDP_sock, struct sockaddr_in broad_addr, int UDP_port){
    char* str = "Жду сообщений!";
    int count_msg; //количество сообщений в очереди
    //начинаем отправлять сообщения
    while (1)
    {
        count_msg = shm_work(10); //получаем количесвто сообщений в очереди
        if (count_msg < msg_max) {
            //Отправляем сообщение "Жду сообщений!"
            printf("Отправляю %s\n", str );
            for (int i = 0; i < max_connect; i++)
            {
                broad_addr.sin_port = htons(UDP_port + i);
                if (sendto(UDP_sock, str, strlen(str), 0, (struct sockaddr*) &broad_addr, sizeof(broad_addr)) != strlen(str))
                    error("sendto failed()");
            }
            printf("Сплю первый тип\n");
            sleep(sleep_client1);
        }
    }
}

void broad_have_msg(int UDP_sock, struct sockaddr_in broad_addr, int UDP_port){
    char* str = "Есть сообщения!";
    int count_msg; //количество сообщений в очереди
    //начинаем отправлять сообщения
    while (1)
    {
        count_msg = shm_work(10); //получаем количесвто сообщений в очереди
        if (count_msg > 0) {
            //Отправляем сообщение "Есть сообщения!"
            printf("Отправляю %s\n", str );
            for (int i = 0; i < max_connect; i++)
            {
                broad_addr.sin_port = htons(UDP_port + i);
                if (sendto(UDP_sock, str, strlen(str), 0, (struct sockaddr*) &broad_addr, sizeof(broad_addr)) != strlen(str))
                    error("sendto failed()");
            }
            printf("Сплю второй тип\n");
            sleep(sleep_client2);
        }
    }
}

int main( int argc, char** argv) {

    if (argc < 4)
        error("Usage: ./server broad_IP TCP_port UDP_port");

    int TCP_sock; //сокет для TCP-сервера
    int UDP_sock; //сокет для UDP-сервера
    struct sockaddr_in broad_addr;//структура для broadcast
    char* broad_IP = argv[1];
    unsigned short TCP_port = atoi(argv[2]); //порт TCP-сервера
    unsigned short UDP_port = atoi(argv[3]); //порт UDP-сервера
    char buff[buff_size]; //сообщение клиентов
    int msg_len;
    pid_t pid; //PID процесса
    int msgqid; //id очереди

    key = ftok(".", 'm');
    //Создаём очередь
    if ((msgqid = msgget(key, IPC_CREAT|0660)) == -1)
        error("msgget() failed");

    //Создаём семафор для синхронизации работы с разделяемой памятью
    semid = semget(key, 1, 0666 | IPC_CREAT);
    //Установка в семафоре №0 значения "1"
    arg.val = 1;
    semctl (semid, 0, SETVAL, arg);

    //Создание области разделяемой памяти
    if ((shmid = shmget(key, sizeof(int), IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    int tmp = 0;
    tmp = shm_work(0); //задаём начальное значение количества сообщений

    //Создаём сокет TCP
    TCP_sock = TCPSocket_creating(TCP_port);
    //Создаём сокет UDP
    UDP_sock = UDPSocket_creating();

    //Заполнение полей структуры
    memset(&broad_addr, 0, sizeof(broad_addr));
    broad_addr.sin_family = AF_INET;
    broad_addr.sin_addr.s_addr = inet_addr(broad_IP); //IP-адрес
    broad_addr.sin_port = htons(UDP_port); //Порт

    //Запускаем бесконечные UDP-сообщения
    if ((pid = fork()) < 0)
        error("fork() failed");
    else if (pid == 0) {
        broad_wait_msg(UDP_sock, broad_addr, UDP_port);
    }

    if ((pid = fork()) < 0)
        error("fork() failed");
    else if (pid == 0)
        broad_have_msg(UDP_sock, broad_addr, UDP_port);

    int client_tcp_sock; //сокет для клиента по tpc
    while (1) {
        //ждём подключение клиента
        client_tcp_sock = Accept_TCPConnection(TCP_sock);

        //получаем от клиента его тип
        memset(buff, 0, sizeof(buff));
        if ((msg_len = recv(client_tcp_sock, buff, buff_size, 0)) < 0)
                error("recv() failed");

        //работу каждого клиента реализуем в порождённом процессе
        if ((pid = fork()) < 0)
            error("fork() failed");
        else if (pid == 0) {
            //close(TCP_sock); //закрываем TCP-сокет, нужно ли закрывать UDP-сокет тоже?
            if (buff[0] == '1')
                client_type_one(client_tcp_sock, msgqid ); //функция работы с клиентом первого типа
            else if (buff[0] == '2')
                client_type_two(client_tcp_sock, msgqid ); //функция работы с клиентом второго типа
            else printf("Какое-то дерьмо!\n");
        }
    }
}
