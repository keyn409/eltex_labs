#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/configfs.h>
#include <linux/init.h>
#include <linux/tty.h> /* For fg_console, MAX_NR_CONSOLES */
#include <linux/kd.h> /* For KDSETLED */
#include <linux/vt.h>
#include <linux/console_struct.h> /* For vc_cons */
#include <linux/vt_kern.h>

#define ALL_LEDS_ON 0x07
#define RESTORE_LEDS 0xFF
//Количество символов в файле /proc/hello (включая символ конца строки)
#define on 2
#define off 3

MODULE_DESCRIPTION("Example module illustrating the use of Keyboard LEDs.");
MODULE_AUTHOR( "dizzy and Keyn" );
MODULE_LICENSE("GPL");

struct tty_driver *my_driver;

int len,temp;

char *msg;

//All up
static void DJ_up(void){
  int i;
  printk(KERN_INFO "kbleds: Up\n");
  for (i = 0; i < MAX_NR_CONSOLES; i++) {
    if (!vc_cons[i].d)
      break;
  }
  my_driver = vc_cons[fg_console].d->port.tty->driver;
  (my_driver->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED, ALL_LEDS_ON);
}

//All down
static void DJ_down(void) {
  printk(KERN_INFO "kbleds: Down\n");
  (my_driver->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED, RESTORE_LEDS);
}

void disco(void){
  if (len == on)
    DJ_up();
  else if (len == off)
    DJ_down();
  else
    printk(KERN_INFO "kbleds: non information len\n");
}

ssize_t read_proc(struct file *filp, char *buf, size_t count, loff_t *offp) {
  if(count>temp) {
    count=temp;
  }

  temp=temp-count;
  copy_to_user(buf, msg, count);
  if(count==0)
    temp=len;
  disco();
  return count;
}

ssize_t write_proc(struct file *filp, const char *buf, size_t count, loff_t *offp) {
  copy_from_user(msg,buf,count);
  len=count;
  temp=len;
  disco();
  return count;
}

struct file_operations proc_fops = {
  read: read_proc,
  write: write_proc
};

void create_new_proc_entry(void) {
  proc_create("hello", 0, NULL, &proc_fops);
  msg = kmalloc(10*sizeof(char), GFP_KERNEL);
}

static int __init hello_module(void) {
  //create proc
  create_new_proc_entry();
  printk(KERN_INFO "kbleds: disco started\n");
  return 0;
}

static void __exit hello_exit(void) {
  printk(KERN_INFO "kbleds: disco has been ended\n");
  DJ_down();
  remove_proc_entry("hello", NULL);
  kfree(msg);
}

module_init(hello_module);
module_exit(hello_exit);
