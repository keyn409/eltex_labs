/*
Сетевой фильтр, который блокирует определённые IP-адреса и порты
Данные(IP-адрес или порт) вводятся клиентом через /proc
*/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netfilter_ipv4.h>
#include <linux/skbuff.h>
#include <linux/ip.h>
#include <linux/inet.h>
#include <linux/tcp.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/uaccess.h>
#include <linux/slab.h>

MODULE_AUTHOR("dizzy and Keyn");
MODULE_DESCRIPTION("Basic netfilter module");
MODULE_LICENSE("GPL");

static struct nf_hook_ops nfin;

int len,temp;

char* msg; //сообщение от пользователя, получаем через proc

//функция блокирования определённого IP-адреса и порта
static unsigned int hook_func_in(void *priv, struct sk_buff *skb, const struct nf_hook_state *state){
   //Структура заголовка IP
   struct iphdr *ip_header;
   //Структура заголовка TCP
   struct tcphdr *tcp_header;

   //инициализируем заголовки
   ip_header = (struct iphdr *)skb_network_header(skb);
   tcp_header = (struct tcphdr *)skb_transport_header(skb);

   char buffer_ip[len]; //ip-адрес отправителя
   char buffer_block[len]; //значение, которое будем блокировать(порт или IP-адрес)

   //Перевод IP-адреса в тип chat
   snprintf(buffer_ip, len, "%pI4", &ip_header->saddr);
   //Считываем только полезную информацию из файла proc, len-длина полезной информации (отбрасываем мусор)
   snprintf(buffer_block, len, "%s", msg);

   //Преобразуем char в int. При преобразование значение по какой-то прицине увеличивается на 48, поэтому вычитаем 48
   int i=0, block_port = 0;
   while(i<len-1){
      block_port = block_port*10 + (int)buffer_block[i] - 48;
      i++;
   }

   //Проверка на IP-адрес. Если совпадают, то блокируем
   if(strcmp(buffer_block, buffer_ip) == 0){
      printk(KERN_INFO "%s blocked\n", buffer_block);
      return NF_DROP;
    } //Если IP-адреса не совпали, значит, сравниваем порты. Если совпали - блокируем порт
   else if (block_port == htons(tcp_header->source)) {
        printk(KERN_INFO "port %d blocked\n", block_port);
        return NF_DROP;
   } //Иначе пропускаем пакет
   else
      return NF_ACCEPT;
}

//инициализация сетевого фильтра
static int fl_init(void) {
   nfin.hook     = hook_func_in;
   nfin.hooknum  = NF_INET_PRE_ROUTING;
   nfin.pf       = PF_INET;
   nfin.priority = NF_IP_PRI_FIRST;
   nf_register_net_hook(&init_net, &nfin);

   return 0;

}

//Отправляем информацию пользователю
ssize_t read_proc(struct file *filp, char *buf, size_t count, loff_t *offp) {
   if(count>temp) {
      count=temp;
   }

   temp=temp-count;
   copy_to_user(buf, msg, count);
   if(count==0)
      temp=len;

   return count;
}

//Получаем от пользователя IP-адрес либо порт, который хотим блокировать
ssize_t write_proc(struct file *filp, const char *buf, size_t count, loff_t *offp) {
   copy_from_user(msg,buf,count);
   len=count;
   temp=len;
   printk(KERN_INFO "proc: User give a source: %s\n", msg);
   return count;
}

//инициализируем операции чтения и записи для proc
struct file_operations proc_fops = {
   read: read_proc,
   write: write_proc
};

//создаем proc
void create_new_proc_entry(void) {
   proc_create("blocker", 0, NULL, &proc_fops);
   msg=kmalloc(16*sizeof(char), GFP_KERNEL);
}

static int __init init_main(void){
   create_new_proc_entry();
   fl_init();
   return 0;
}

static void __exit cleanup_main(void){
   nf_unregister_net_hook(&init_net, &nfin);
   remove_proc_entry("blocker", NULL);
   kfree(msg);

}

module_init(init_main);
module_exit(cleanup_main);
