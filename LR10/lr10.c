#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
//параметры для теста: ./lr10 5 -1 -1 5

int chance; //для генерации случайных чисел

struct BALL{
    int x;
    int y;
    int number;
    int life; //идентификатор жизни
};

struct randomization{
    pthread_mutex_t mutex;
    int probability;
    int sign;
} randomize = {
    PTHREAD_MUTEX_INITIALIZER
};

typedef struct BALL ball;

void *thread_func (void *arg) {
    ball tmp = * (ball *) arg;
    //блокируем критическую область
    pthread_mutex_lock(&randomize.mutex);
    //генерируем значение по оси X
    randomize.sign = rand() % 2;
    randomize.probability = rand() % chance;
    if (randomize.sign == 1)
        tmp.x += randomize.probability;
    else
        tmp.x -= randomize.probability;
    //генерируем значение по ису Y
    randomize.sign = rand() % 2;
    randomize.probability = rand() % chance;
    if (randomize.sign == 1)
        tmp.y += randomize.probability;
    else
        tmp.y -= randomize.probability;
    //разблокируем критическую область
    pthread_mutex_unlock(&randomize.mutex);
    * (ball *) arg = tmp;
    sleep(1);
    pthread_exit(NULL);
}

int main (int argc, char **argv)
{
    if ( argc < 5) {
        printf("Usage: ./lr10 count_balls limit_x limit_y rand_value\n");
        exit(1);
    }
    int count = atoi(argv[1]); //количество шариков
    int limit_x = atoi(argv[2]); //граница по оси x
    int limit_y = atoi(argv[3]); //граница по оси y
    chance = atoi(argv[4]); //случайная величина для rand()
    //проверка на количество шариков
    if (count <=0 ){
        printf("Количество шариков должно быть больше нуля!\n");
        exit(1);
    }
    ball* b1 = NULL;
    //создание потоков
    int result, i;
    pthread_t thread1[count];
    //выделяе память для мячей
    b1 = (ball*) malloc(sizeof(ball)*count);
    //задание начальных параметров
    for (i=0; i< count; i++) {
        b1[i].x = 0;
        b1[i].y = 0;
        b1[i].number = i;
        b1[i].life = 1;
    }
    int chet = 0; //счетчик мёртвых мячей
    //считываем данные и проверяем значения
    while (1) {
        if (chet == count)
            break;
            //создаём потоки
        for (i =0; i< count; i++)
        {
            srand(time(NULL));
            result = pthread_create(&thread1[i], NULL, thread_func, &b1[i]);
            if (result != 0) {
                perror("Ошибка при создании потока");
                exit(1);
            }
        }
        //обрабатываем результаты работы потоков
        for (i = 0; i< count; i++){
            result = pthread_join(thread1[i], NULL);
            if (result != 0) {
                perror("Join error");
                exit(1);
            } else {
                if (b1[i].life == 1) {
                    printf("Параметры мяча № %d:X = %d, Y = %d\n",b1[i].number, b1[i].x, b1[i].y);
                    if ( b1[i].x <= limit_x || b1[i].y <= limit_y){
                        printf("Мяч №%d умер!\n",i );
                        b1[i].life = 0;
                        chet++;
                    } else {
                        printf("Мяч №%d жив!\n", i );
                    }
                }
                else continue;
            }
        }
        printf("\n");
    }
    printf("Все мячи умерли!\n");
    free(b1);
    return 0;
}
