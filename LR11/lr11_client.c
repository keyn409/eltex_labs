#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */

#define buff_size 256

//ошибки
void error(char* str){
    perror(str);
    exit(1);
}

//Создание сокета и установление соединения
int TCPSocket_creating (unsigned short port_number, char* server_IP) {
    printf("Создание сокета клиента\n");
    int sock;
    struct sockaddr_in serv_addr;
    //Создание сокета
    if ((sock = socket(AF_INET, SOCK_STREAM,0)) < 0)
        error("socket");

    //Заполняем структуру serv_addr
    memset(&serv_addr, 0, sizeof(serv_addr)); //зануляем структуру
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(server_IP); //IP-адрес сервера
    serv_addr.sin_port = htons(port_number);//Порт
    //установка соединения
    if (connect(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        error("connect");
    return sock;
}

//работа клиента с сервером
void client_work(int sock) {
    char buff[buff_size]; //буфер для сообщений
    unsigned int msg_len; //длина сообщений
    memset(buff, 0, sizeof(buff)); //занулим буффер
    printf("Здравствуйте. Введите имя файла, в котором необходимо найти строку.\n");
    scanf("%s", buff);
    //Отправляем серверу имя файла
    if (send(sock, buff,buff_size, 0) != buff_size)
        error("send");

    memset(buff, 0, sizeof(buff)); //занулим буффер
    printf("Введите строку для поиска.\n");
    fgets (buff, buff_size, stdin); //необходимо для первого считывания просто так, без этой строки не работает
    fgets (buff, buff_size, stdin);
    buff[strlen(buff) - 1] = '\0';
    //Отправляем серверу строку для поиска
    if (send(sock, buff,buff_size, 0) != buff_size)
        error("send");

    sleep(1);
    memset(buff, 0, sizeof(buff)); //занулим буффер

    //Принимаем информацию от сервера
    if ((msg_len = recv(sock, buff, buff_size - 1, 0)) <= 0)
        error("recv()");
    //Работаем с полученной информацией, первый элемент массива - количество строк, далее - номера найденных строк, в конце - -1;
    int a = 0; //переменная-счетчик
    if (buff[0] == 0) {
        printf("Не найдено ни одной строки. Спасибо за работу!\n");
    } else {
        printf("Количество строк - %d\n", buff[0]);
        a = 1;
        printf("Номера: ");
        while (buff[a] != -1) {
            printf("%d; ", (int)buff[a] );
            a++;
        }
        printf("\n");
    }
    close(sock);
}

int main(int argc, char** argv){
   if (argc < 3){
        printf("Usage: ./lr11_client Server_IP Server_Port\n");
        exit(1);
    }
    int sock; //сокет клиента
    unsigned short port_number; //порт
    char* server_IP; //IP-адрес сервера
    server_IP = argv[1];
    port_number = atoi(argv[2]);
    sock = TCPSocket_creating(port_number, server_IP);
    client_work(sock);
    return 0;

}
