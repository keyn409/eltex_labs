#include <string.h>     /* for memset() */
#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), bind(), and connect() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */

#define max_connect 5
#define buff_size 256

//ошибки
void error(char *message)
{
    perror(message);
    exit(1);
}
//создание сокета и привязывание адреса
int TCPServer_creating(unsigned short port) {
    int sock;
    struct sockaddr_in serv_addr;

    //Создание сокета
    if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP )) < 0)
        error("socket");

    //Заполнение полей структуры
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(port);

    //Привязываем сокет к локальному адресу
    if (bind(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        error("bind");

    //Выставляем сокет на прослушку входящих соединений
    if (listen(sock, max_connect) < 0)
        error("listen");
    printf(" Сервер готов! Параметры: IP-адрес - %s, Порт - %d\n", inet_ntoa(serv_addr.sin_addr), port );
    return sock;
}
//подключаем клиента
int Accept_TCPConnection(int serv_sock) {
    int client_sock; //Сокет для клиента
    struct sockaddr_in client_addr; //Адрес клиента
    unsigned int client_len; //Размер структуры адреса клиента

    //Определяем размер структуры клиента
    client_len = sizeof(client_addr);

    //Ожидаем подключение от клиента
    if ((client_sock = accept(serv_sock, (struct sockaddr *) &client_addr, &client_len)) < 0)
        error("Accept");
    printf("Подключился клиент [%s]!\n", inet_ntoa(client_addr.sin_addr) );
    return client_sock;
}

void TCP_client_work(int client_sock)
{
    char buff[buff_size];
    int msg_len; //длина сообщения
    FILE* f1;
    char *ukaz; //указатель, необходимый для проверки конца файла
    int number = 0; //номер строки
    int count = 0; //количество найденных строк
    int result[buff_size]; //массив, в который записываем номера строк
    char test[buff_size]; //сюда считываем строки из файла
    char str[buff_size]; //строка, которую ищем

    printf("Начинаем работу с клиентом!\n");
    //принимаем имя файла
    if ((msg_len = recv(client_sock, buff, buff_size, 0)) < 0)
        error("recv");
    buff[strlen(buff)] = '\0'; //указываем конец строки
    printf("Сервер получил имя файла - %s\n", buff);
    //Открываем полученный файл
    f1 = fopen(buff,"r");
    if (f1 == NULL)
        error("Файл не открылся");

    //принимаем строку, которую нужно найти
    if ((msg_len = recv(client_sock, buff, buff_size, 0)) < 0)
        error("recv");
    buff[strlen(buff)] = '\n'; //указываем конец строки
    printf("Сервер получил строку для поиска - %s", buff);
    //копируем искомую строку в переменную
    strcpy(str, buff);

    printf("Выполняем поиск...\n");
    while (1) {//считываем файл построчно;
        ukaz = fgets(test, buff_size, f1);
        if (ukaz == NULL )
            break; //достигли конца файла
        number++;
        if (strcmp(str,test) == 0) {
            result[count] = number;
            count++;
            printf("Обнаружено совпадение, номер строки - %d\n", number );
        } else {
            printf("В строке под номер %d совпадений не обнаружено\n", number );
        }
    }
    printf("\n");
    printf("Количество найденных строк - %d\n", count);
    //Готовим буффер для отправки
    memset(buff, 0, sizeof(buff)); //занулим буффер
    buff[0] = count; //количество найденных строк
    int i;
    int chet = 0;
    for (i = 1; i < count + 1; i++) {
        buff[i] = (char)result[chet];
        chet++;
    }
    buff[i] = -1; //конец полезной информации
    //Отправляем инфомрацию о работе
    if (send(client_sock, buff,buff_size, 0) != buff_size)
        error("send");
    printf("Завершение соединения, Клиент - Пока!\n");
    fclose(f1);
}


int main(int argc, char **argv)
{
    if (argc < 2){
        printf("Usage: ./lr11 number_of_port\n");
        exit(1);
    }
    int serv_sock; //Сокет сервера
    unsigned short serv_port; //Порт сервера

    //Присваиваем начальные значения
    serv_port = atoi(argv[1]);

    //Вызываем функция создания сокета для сервера
    serv_sock = TCPServer_creating(serv_port);

    int client_sock; //создаём сокет для клиента
    pid_t pid; //PID процесса
    while (1) //бесконечный цикл
    {
        sleep(1);
        client_sock = Accept_TCPConnection(serv_sock);
        printf("PID клиента: %d\n", getpid());
            //начинаем работу с процессами; Количество процессов равно количеству команд
        if ((pid = fork()) < 0)
            error("fork");
        else if (pid == 0) {
            close(serv_sock);
            TCP_client_work(client_sock);
            return 0;
        }
    }
    exit(0);
}
