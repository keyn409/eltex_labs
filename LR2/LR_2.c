#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h> //для функции isdigit
#define max 500

int sum = 0; //количество перестановок

//Ввод массива
char **input_mas (int count)
{
	char **mas; //создаём массив строк
	char stroka[max];//создаём массив для единичной строки
	mas = (char **)malloc(sizeof(char*)*count); //выделяем память под массив строк
	fgets (stroka, max, stdin); //необходимо для первого считывания просто так, без этой строки не работает
	for (int i=0; i<count;i++)
	{
		printf ("Введите строку;\n");
		fgets (stroka, max, stdin);
		mas[i] = (char *)malloc(sizeof(char)*strlen(stroka)); // выделяем память для единичной строки
		strcpy(mas[i], stroka); // копируем введённую строку в массив
	}
	return mas;
}
//Вывод массива на экран
void print_mas (char **mas, int count)
{
	for (int i=0;i < count; i++)
		printf ("%d)%s\n",i+1, mas[i]);
}
//Вариант 2 - расположение строк по убыванию длины

//для функции qsort
int for_dlin_down(const void *tmp1, const void *tmp2)
{
	char **p1 = (char**)tmp1;
	char **p2 = (char**)tmp2;
	sum++;
	return (strlen(*p1) - strlen(*p2))*(-1);
}

void delete (char **mas,int count)
{
	for (int i=0; i < count; i++)
		free(mas[i]);

	free (mas);
}

int main(int argc, char **argv)
{
	char **mas = NULL;
	int count = 0;
	printf ("Введите количество строк в массиве:\n");
	scanf ("%d", &count);
	mas = input_mas(count);
	printf ("Изначальный массив:\n");
	//Выводим массив на экран
	print_mas(mas,count);
	printf("\n");
	//сортировка
	qsort( &mas[0], count, sizeof(char*), for_dlin_down);
	printf ("Отсортированный массив:\n");
	//Выводи отсортированный массив
	print_mas(mas,count);
	printf("\n");
	printf ("Количество перестановок - %d;\nДлина большей строки - %ld;\n", sum, strlen(mas[0]));
	//Очищаем память
	delete(mas,count);
	return 0;
}
