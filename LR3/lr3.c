#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mcheck.h>
#define max 100

//Объявляем структуру деталей
struct details {
	char* soname;
	int year;
	int cost;
	int count;
};

typedef struct details det;

//Функция ввода информации о детале
det** input(det **tmp, int sum)
{
	for (int i=0;i < sum;i++) {
 		printf("Введите название детали:\n");
		scanf("%s",tmp[i]->soname);
		printf("Введите год выпуска:\n");
		scanf("%d",&tmp[i]->year);
		printf("Введите стоимость:\n");
		scanf("%d",&tmp[i]->cost);
		printf("Введите количество:\n");
		scanf("%d",&tmp[i]->count);
		printf("\n");
	}
	return tmp;
}
//Вывод на экран
void output(det **tmp, int sum)
{
	for(int i=0;i < sum; i++) {
		printf("Деталь №%d:\n",i+1);
		printf("Название детали - %s\n",tmp[i]->soname);
		printf("Год выпуска - %d\n",tmp[i]->year);
		printf("Стоимость - %d\n",tmp[i]->cost);
		printf("Количество - %d\n",tmp[i]->count);
		printf("\n");
	}
}
//Для функции qsort
int for_sort(const void *p1, const void *p2)
{
	det **tmp1 = (det**) p1;
	det **tmp2 = (det**) p2;
	return (*tmp1)->cost - (*tmp2)->cost;
}
//Очистка памяти
void delete(det **mas,int sum)
{
	for (int j=0; j<sum;j++) {
		free(mas[j]->soname);
		free(mas[j]);
	}
	free(mas);
}

int main(int argc, char **argv)
{
	det **mas = NULL;
	int sum=0;
	printf("Введите количество разновидностей деталей\n");
	scanf("%d",&sum);
	//Выделяем память
	mas = (det**)malloc(sizeof(det*)*sum);
	for (int i=0; i< sum; i++) {
		mas[i] = (det*)malloc(sizeof(det*));
		mas[i]->soname = (char*)malloc(sizeof(char)*max);
	}
	mas=input(mas,sum);
	output(mas,sum);
	//Сортируем
	qsort(&mas[0],sum,sizeof(det*),for_sort);
	printf ("Отсортированные детали по цене:\n");
	output(mas,sum);
	//Очищаем память
	delete(mas,sum);
	return 0;
}
