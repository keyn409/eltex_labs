#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h> // для функции isalpha
#define max 100

//Вариант 2 - Оставить только строки, начинающиеся с латинских букв

int main(int argc, char **argv)
{
    if (argc < 3) {
    	fprintf(stderr, "Usage: ./lr4_1 filename count_of_strings \n");
    	exit(1);
    }

    char* newfile = NULL;
    FILE *f1;
    char test[100];
    char* ukaz; //указатель, необходимый для проверки конца файла
    FILE *f2;
    //определение расширения файла и задание имени для нового файла
    int dlin = strlen(argv[1]);
    int i = 0;

    //Если файл формата txt, то преобразуем в формат odt
    if ((argv[1])[dlin - 1] == 't' && (argv[1])[dlin - 2] == 'x' && (argv[1])[dlin - 3] == 't') {
    	newfile = (char*)malloc(sizeof(char)*dlin);
    	strncat (newfile, argv[1],dlin -3);
    	strcat(newfile, "odt");
    } else { //иначе, преобразуем в формат txt
    	i=0;
    	while ((argv[1])[i] != '.')
    			i++;

    	newfile = (char*)malloc(sizeof(char)*(i+4));
    	strncat (newfile, argv[1],i);
    	strcat(newfile, ".txt");
    }
    //Открываем файлы
    f1 = fopen(argv[1], "r");
    if ( f1 == NULL) {
    	perror("Ошибка при открытии файла f1");
    	exit(1);
    }

    f2 = fopen(newfile, "w");
    if ( f2 == NULL) {
        perror("Ошибка при открытии файла f2");
        exit(1);
    }
    for (int i =0 ; i < atoi(argv[2]); i++) {
    	ukaz = fgets(test,max,f1);
    	//Проверка на конец файла или ошибку чтения
        if (ukaz == NULL)
            break;
      	//Сравниние с длиной и запись в файл
      	if (isalpha(test[0])) // проверка на английскую букву
       		fputs(test, f2);
      	else continue;
    }
    //Закрываем файлы
    fclose(f1);
    fclose(f2);
    free(newfile);
    return 0;
}
