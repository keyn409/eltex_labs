#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define filename "tmp.txt" //файл, с которым будем работать
//Вариант 2 - в конце каждой строки вставить заданный символ

int main(int argc, char **argv)
{
    if (argc < 3) {
    	fprintf(stderr, "Usage: ./lr4_2 filename simbol\n");
    	exit(1);
    }

    char* newfile = NULL;
    FILE *f1;
    char test[100]; //попробовать сделать тут динамическое выделение памяти
    FILE *f2;
    //определение расширения файла и задание имени для нового файла
    f1 = fopen(argv[1], "r");

    if ( f1 == NULL) {
        perror("Ошибка при открытии файла f1");
        exit(1);
    }

    f2 = fopen(filename, "w");
    if ( f2 == NULL) {
        perror("Ошибка при открытии файла f2");
        exit(1);
    }
    int i=0;
    unsigned long ukaz;
    while (1) {
    	ukaz = fread(test, sizeof(char),1, f1);
    	//Проверка на конец файла или ошибку чтения
        if (ukaz == 0)
            break;
        //Сравниние с длиной и запись в файл
        if (strcmp(test,"\n") == 0) {
            i++;
            fputs(argv[2], f2);
            fputs(test, f2);
        }
        else fputs(test, f2);
    }

    printf("%d\n",i );
    fclose(f1);
    fclose(f2);
    rename(filename, argv[1]);//переименование нового файла в имя старого
    free(newfile);
    exit(EXIT_SUCCESS);
}
