#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    int a=0;
    int b=0;
    int choice = 0;
    printf("Введите первое число\n");
    scanf ("%d",&a);
    printf("Введите второе число\n");
    scanf ("%d",&b);
    printf("Выберите операцию\n 1)Умножение;\n 2)Деление;\n");
    scanf ("%d",&choice);

    void *lib_test;
    lib_test = dlopen("../Library/libmy2.so", RTLD_LAZY); //Подключаем библиотеку
    if (!lib_test) { //если ошибка
        fprintf(stderr,"dlopen() error: %s\n", dlerror());
        exit(1);
    }

    int (*ufunc)(int a,int b);
    switch(choice)
    {
        case 1:
        {
            ufunc = dlsym(lib_test,"multiply");
            printf("a*b=%d\n",(*ufunc)(a,b));
            break;
        }
        case 2:
        {
            ufunc = dlsym(lib_test,"devide");
            printf("a/b=%d\n",(*ufunc)(a,b));
            break;
        }
        default:
        {
            printf("Выбрана неверная операция!\n");
            break;
        }
    }
  dlclose(lib_test); //отключаем библиотеку
  return 0;
}
