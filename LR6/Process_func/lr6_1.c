#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{
	if (argc < 3) {
		printf("Usage: ./lr6_1 count_of_strings count_of_columns\n");
		exit(1);
	}
	int str = atoi(argv[1]);
	int column = atoi(argv[2]);
	pid_t pid;
	//выделение памяти для матрицы
	int **matrix = NULL;
	matrix = (int**)malloc(sizeof(int*)*str);
	//выделение памяти для вектора
	int *vektor = NULL;
	vektor = (int*)malloc(sizeof(int)*column);
	//выделение памяти для результата перемножения матрицы на вектор
	int *result = NULL;
	result = (int*)malloc(sizeof(int)*str);
	int number = 0; //для возвращения результата перемножения из функции
	int chet = 0; //переменная-счетчик
	int i;
	printf("Умножение матрицы на вектор.\nДля верного умножения количество столбцов матрицы должно быть равно количеству строк в векторе-столбце\n");

	//Ввод значений матрицы
	for (i=0; i < str;i++) {
		matrix[i] = (int*)malloc(sizeof(int)*column);
		for (int j=0; j < column; j++) {
			printf("Введите %d элемент %d-ой строки\n", j+1, i+1);
			scanf("%d",&matrix[i][j]);
		}
	}

	//Ввод значений вектора-столбца
	for (i = 0; i < column; i++) {
		printf("Введите значение %d-го элемента столбца\n", i+1);
		scanf("%d", &vektor[i]);
	}
	//Вывод на экран введённый матрицы
	printf("Матрица имеет следующий вид:\n");
	for (i=0; i < str;i++) {
		printf("( ");

		for (int j=0; j < column; j++)
			printf("%d ", matrix[i][j]);

		printf(")\n");
	}
	printf("Вектор имеет следующий вид:\n");
	//Вывод на экран вектора-столбца:
	for (i = 0; i< column; i++)
		printf("( %d )\n", vektor[i]);
	//перемножение с использованием дочернего процесса - функции
	for (int i=0 ; i < str; i++)
	{
		pid = fork();
		switch(pid)
		{
			case -1:
				perror("fork");
				exit(1);
			case 0: //действия потомка
				printf("Процесс потомок\n");
				printf("Мой PID - %d\n", getpid());
				printf("PID родителя - %d\n", getppid() );
				//Выпоняем умножение строки на вектор
				for (int j=0; j< column; j++)
					number = number + matrix[chet][j]*vektor[j];

				printf("Ребёнок закончил\n");
				return number;
			default: //действия родителя
				printf("Процесс родитель\n");
				printf("Мой PID - %d\n", getpid());
				printf("PID потомка - %d\n", pid);
				wait(&number);
				result[i] = WEXITSTATUS(number);
				printf("Родитель закончил\n");
				printf("\n");
		}
		chet++;
	}
	printf("Выполняем умножение. Результат:\n");
	for (int i = 0; i< chet; i++)
		printf("( %d )\n", result[i]);

	//Освобождение памяти
	for (i=0; i < str;i++)
		free(matrix[i]);

	free(matrix);
	free(vektor);
	free(result);
	return 0;
}
