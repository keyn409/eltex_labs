#include <stdio.h>
#include <stdlib.h>
#define filename "tmp.txt"
int main(int argc, char *argv[])
{
	int result = 0;
	int stolb = atoi(argv[2]);
	int *matrix = (int*)malloc(sizeof(int)*stolb);
	int *vektor = (int*)malloc(sizeof(int)*stolb);
	//открытие файла, в котором сохранена строка и вектор
	FILE *f1;
	f1 = fopen(filename, "r");
	int i=0;
	for (i=0; i < stolb; i++)
	{
		fscanf(f1,"%d", &matrix[i]);
	}
	for (i=0; i < stolb; i++)
	{
		fscanf(f1,"%d", &vektor[i]);
	}
	fclose(f1);
	//умножение
	for (i=0; i< stolb; i++)
	{
		result = result + matrix[i]*vektor[i];
	}
  	printf("Результат ребёнка - %d\n", result);
  	free(matrix);
  	free(vektor);
	return (result);
}