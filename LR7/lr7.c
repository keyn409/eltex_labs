#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int child ( int fd[], int diap[], int i, int len ) { //процесс - ребёнок
    printf("Процесс потомок №%d вступил в работу\n", i );
    close(fd[0]); //закрываем доступный для чтения конец канал
    //проверяем диапазон на простые числа
    int chisl[len];
    int count = 0;
    int sum = 0; //количество простых чисел
    //определяем простые числа из диапазона
    for (int j = diap[i]; j<= diap[i+1]; j++) {
        count =0;
        for (int s=2; s < j; s++) {
            if (j % s == 0)
                count ++;
        }
        if (count == 0) {
                chisl[sum] = j;
                sum++;
            }
    }
    //записываем в канал
    write(fd[1], chisl, sizeof(chisl));
    return sum;
}

int main(int argc, char *argv[])
{
    if (argc < 3) {
        printf("Usage: ./lr7 first_number last_number\n");
        exit(-1);
    }

    int *result = NULL; //простые числа
    int *diap = NULL;//диапазоны
    int first, last, chet, len, i, razm;
    //Получаем данные типа int
    first = atoi(argv[1]);
    last = atoi(argv[2]);
    len = last - first + 1;
    //Выделяем память
    diap = (int*)malloc(sizeof(int)*len);
    result = (int*)malloc(sizeof(int)*len);
    //ввод диапазонов - от первой до цифры до diap[0], от diap[0] до diap[1], ... до последней цифры
    chet = 0;
    razm = 1;
    diap[0] = first;
    //определяем диапазоны
    while(1) {
        printf("Введите диапазон; Для прекращения ввода введите 0;\n");
        scanf("%d", &chet);
        if (chet == 0) {
            diap[razm] = last;
            razm++;
            break;
        }
        else {
            diap[razm]=chet;
            razm++;
        }
    }

    //объявляем и запускаем процессы и каналы
    int fd[razm-1][2];
    pid_t pid[razm-1];
    int stat, status, sum;
    for (i = 0; i < razm-1; i++) {
        pipe(fd[i]);
        pid[i] = fork();
        switch(pid[i]) {
            case -1:
                perror("fork");
                exit(1);
            case 0:
                sum = child(fd[i], diap, i, len);
                exit(sum);

        }
    }
    chet = 0;
    printf("Это процесс родитель!\n");
    for (i = 0; i < razm-1; i++)
    {
        status = waitpid(pid[i], &stat, 0 );
        if (pid[i] == status)
        {
            printf("процесс-потомок %d done,  result=%d\n", i, WEXITSTATUS(stat));
            close(fd[i][1]); //закрываем доступный для записи конец канала
            int test[len];
            //считываем с канала
            read(fd[i][0], test, sizeof(test));
            //Добавляем простые числа в массив
            for (int q = 0; q < WEXITSTATUS(stat); q++) {
                if (result[chet-1] == test[q])
                    continue;
                else {
                    result[chet] = test[q];
                    chet++;
                }
            }
        }
    }
    //Вывод результатов
    printf("Всего простых чисел - %d\n",chet );
    for (i=0; i< chet; i++)
        printf("%d ",result[i] );

    printf("\n");
    //Очистка памяти
    free(result);
    free(diap);
    return 0;
}
