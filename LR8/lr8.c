#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>

struct message_unit { //структура содержит параметры одного юнита
	long mtype;
	int portion; //порция золота
	int delay; //время задержки
	int number; //номер юнита
};

typedef struct message_unit unit;

int main (int argc, char **argv)
{
	if (argc < 4)
	{
		printf("Usage: ./lr8 portion size_of_mine count_unit\n");
		exit(2);
	}
	int i, msgqid, rc;
	unit warcraft;
	int mine = atoi(argv[2]); //размер шахты
	int count_unit = atoi(argv[3]); // количество юнитов
	pid_t pid[count_unit]; //количество порожденных процессов
	key_t key;
	key = ftok(".",'m');
	if ((msgqid = msgget(key, IPC_CREAT|0660)) == -1) { //создание очереди
		perror ("msgget");
		exit(2);
	}
	double sum;
	int total; //количество юнитов, которых будем отправлять в шахту
	while (mine > 0) //юниты буду работать по в шахте есть золото
	{
		printf("\n");
		sum = mine / atoi(argv[1]);
		if ( sum >= count_unit) //проверка для определения количества юнитов для отправки
			total = count_unit;
		else if (mine % atoi(argv[1]) == 0)
			total = sum;
			else total = sum + 1;
		for (i = 0; i < total; i++){

			pid[i] = fork();
			srand(getpid());
			if (pid[i] == -1)
			{
				perror("fork"); //ошибка при вызове дочернего процесса
				exit(1);
			} else if (pid[i] == 0) {
				printf("Юнит №%d вступил в работу!\n",i);
				warcraft.portion = atoi(argv[1]);
				int tmp;
				tmp = rand() % 4;
				sleep(tmp);
				printf(" Юнит №%d задержался на %d секунд\n",i, tmp );
				//записываем сообщение в очередь
				warcraft.delay = tmp;
				warcraft.number = i;
				warcraft.mtype = 1;
				if ((msgsnd(msgqid, &warcraft, sizeof(warcraft) - sizeof(long), 0)) == -1) {
					perror("msgsnd");
					exit(1);
				}
				printf("Юнит №%d отправил сообщение!\n", i );
				fflush(stdout);
				return tmp;
			}
		}
		//родительский процесс
		int status, stat;
		printf("Это процесс- родитель! Отправляем юнитов в шахту\n");
		for (i = 0; i < total; i++) {
			status = waitpid(pid[i], &stat, 0);
			if (status == pid[i]){
				printf("Юнит %d закончил, вернул значение - %d\n", i, WEXITSTATUS(stat));
				fflush(stdout);
			}
		}
		for (i = 0; i < total; i++)
		{
			warcraft.mtype = 1;
			msgrcv(msgqid, &warcraft, sizeof(warcraft) - sizeof(long), warcraft.mtype, 0 );
			printf("Юнит №%d вынес %d золота,  задержался на %d секунд\n", warcraft.number, warcraft.portion, warcraft.delay );
			mine = mine - warcraft.portion;
			if (mine < 0 )
				mine = 0;
			printf("	В шахте осталось %d золота\n", mine );
			if (mine <= 0)
			{
				printf("Шахта истощена!\n");
				if ((rc = msgctl(msgqid, IPC_RMID, NULL)) < 0){ //закрываем очередь
					perror( strerror(errno));
					printf("Ошибки при выполнении msgqid, rc = %d\n", rc );
					exit(1);
					}
				return 0;
			}
		}
	}
}
