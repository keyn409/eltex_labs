#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>

struct message_unit { //структура содержит параметры одного юнита
    long mtype;
    int portion; //порция золота
    int delay; //время задержки
    int number; //номер юнита
};

typedef struct message_unit unit;

//работа отдельного юнита в виде дочернего процесса
int worker (int gold_unit, unit warcraft, int i, int msgqid, int diff){
    int gold = 0;
    int TIME = 0;
    for (int q = 0; q < diff; q++) {
        printf("Юнит №%d вступил в работу! Мой PID - %d\n",i, getpid());
        gold += gold_unit;
        int tmp;
        tmp = rand() % 4;
        TIME +=tmp;
        sleep(tmp);
        printf(" Юнит №%d задержался на %d секунд\n",i, tmp );
    }
    //записываем сообщение в очередь
    warcraft.delay = TIME;
    warcraft.number = i;
    warcraft.mtype = 1;
    warcraft.portion = gold;
    if ((msgsnd(msgqid, &warcraft, sizeof(warcraft) - sizeof(long), 0)) == -1) {
        perror("msgsnd");
        exit(2);
    }
    printf("Юнит №%d принёс золото!\n", i );
    fflush(stdout);
    return TIME;
}


int main (int argc, char **argv)
{
    if (argc < 4)
    {
        printf("Usage: ./lr8 portion size_of_mine count_unit\n");
        exit(1);
    }
    int i, msgqid, rc;
    unit warcraft;
    int gold_unit = atoi(argv[1]);
    int mine = atoi(argv[2]); //размер шахты
    int count_unit = atoi(argv[3]); // количество юнитов
    pid_t pid[count_unit]; //количество порожденных процессов равно количеству юнитов
    key_t key;
    key = ftok(".",'m');
    if ((msgqid = msgget(key, IPC_CREAT|0660)) == -1) { //создание очереди
        perror ("msgget");
        exit(2);
    }
    double sum;
    int total; //количетсво юнитов, которых будем отправлять в шахту
    double once = gold_unit * count_unit; //сколько всего золота принесут рабочие за раз
    double diff = (double)mine / once; //получаем количество ходок, то есть сколько раз каждый рабочий должен сходить в шахту
    double tmp = diff - (int)diff;
    if (tmp != 0)
        diff++;
    sum = mine / gold_unit;
    if ( sum >= count_unit) //проверка для определения количества юнитов для отправки
        total = count_unit;
    else if (mine % atoi(argv[1]) == 0)
        total = sum;
        else total = sum+1;

    for (i = 0; i < total; i++){
        pid[i] = fork();
        srand(getpid());
        if (pid[i] == -1)
        {
            perror("fork"); //ошибка при вызове дочернего процесса
            exit(1);
        } else if (pid[i] == 0) {
            int result;
            result = worker(gold_unit, warcraft, i, msgqid,diff);
            return result;
        }
    }
    //родительский процесс
    int status, stat;
    printf("Это процесс- родитель! Отправляем юнитов в шахту\n");
    for (i=0; i < total; i++){
        status = waitpid(pid[i], &stat, 0);
        if (status == pid[i]){
            printf("Юнит %d закончил, вернул значение - %d\n", i, WEXITSTATUS(stat));
            fflush(stdout);
        }
    }
    for (i=0; i< total; i++)
    {
        warcraft.mtype = 1;
        msgrcv(msgqid, &warcraft, sizeof(warcraft) - sizeof(long), warcraft.mtype, 0 );
        printf("------->Юнит №%d вынес %d золота,  задержался на %d секунд\n", warcraft.number, warcraft.portion, warcraft.delay );
        mine = mine - warcraft.portion;
        if (mine < 0 )
            mine = 0;
        printf(" В шахте осталось %d золота\n", mine );
        if (mine <= 0)
        {
            printf("Шахта истощена!\n");
            if ((rc = msgctl(msgqid, IPC_RMID, NULL)) < 0){ //закрываем очередь
                perror( strerror(errno));
                printf("Ошибки при выполнении msgqid, rc = %d\n", rc );
                exit(2);
                }
            exit(1);
        }
    }
}
