#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <ctype.h>
#include <signal.h>

//параметры для тестирования: 10 40 4 12(или 13)

union semun { //объединение, необходимое для семаформа
    int val; //значение для SETVAL
    struct semid_ds *buf; //буферы для IPC_STAT и IPC_SEt
    unsigned short *array; //массивы для GATALL и SETALL
    struct seminfo *__buf; //буфер для IPC_INFO
};

struct BEAR {
    int barrel; //бочка
    int time_life; //время жизни медведя
};
//структуры для семафора
struct sembuf lock_res = {0, -1, 0}; //блокировка ресурса
struct sembuf res_res = {0, 1, 0}; //освобождение ресурса

typedef struct BEAR bear;

void child_bee (bear* shm, int shmid, int semid, int i, int portion_bee){
    printf("    Пчела №%d начинает работу! PID = %d\n", i, getpid() );
    int tmp =1 + rand() % 4;
    sleep(tmp);
    //Получение доступа к разделяемой памяти
    if ((shm = (bear*)shmat(shmid, NULL, 0)) == (bear*) -1) {
        perror("shmat");
        exit(1);
    }
    printf("    Пчела №%d задержалась на %d секунд\n", i, tmp);
    //Заблокируем разделяемую память
    if ((semop(semid, &lock_res, 1)) == -1){
        fprintf(stderr, "Semop - Lock failed\n");
        exit(1);
    }
    shm->time_life -= tmp;
    shm->barrel += portion_bee;
    //Освободим разделяемую память
    if ((semop(semid, &res_res, 1)) == -1) {
        fprintf(stderr, "Semop - Unlock failed\n");
        exit(1);
    }
    //Отключаемся от разделяемой памяти
    if (shmdt(shm) < 0){
        perror("shmdt");
        exit(1);
    }
}

int parent (bear* shm, int shmid, int semid, int bear_portion, int bear_time ){
    int result = 1;
    sleep(1);
    //Получение доступа к разделяемой памяти
    if ((shm = (bear*)shmat(shmid, NULL, 0)) == (bear*)-1 ) {
        perror("shmat");
        exit(1);
    }
    //Заблокируем разделяемую память
    if ((semop(semid, &lock_res, 1)) == -1) {
        fprintf(stderr, "Semop - Lock failed\n");
        exit(1);
    }
    printf("----------> Количетство мёда в бочке - %d; TIME_LIFE = %d секунд\n",shm->barrel, shm->time_life );
    if (shm->time_life > 0){
        if (shm->barrel >= bear_portion) {
            shm->barrel -= bear_portion; //вычитаем из бочки мед, который медведь съел
            shm->time_life = bear_time; //приравниваем к времени жизни время поедания порции, так как медведь сыт
            printf("---------->Медведь поел! TIME_LIFE =  %d секунд\n", shm->time_life);
        }
        else printf("---------->Медведь не поел, так как мало мёда. Ждёт следующую пчелу;\n");
        //Освободим разделяемую память
        if ((semop(semid, &res_res, 1)) == -1) {
            fprintf(stderr, "Semop - Unlock failed\n");
            exit(1);
        }
    } else
        result = 0;
    return result;
}

//удаление сегмента памяти и семафора
void clear(int shmid, int semid, pid_t pid[], int count_bee) {
    printf("---------->Медведь умер!\n");
    //заканчиваем все дочерние процессы
    for (int i = 0; i < count_bee; i++)
    kill(pid[i],SIGKILL);
    //Удаление сегмента разделяемой памяти
    if (shmctl(shmid, IPC_RMID, 0) < 0){
        perror("shmctl");
        exit(1);
    }
    //Удаление семафора
    if (semctl(semid, 0, IPC_RMID) < 0){
        perror("semctl");
        exit(1);
    }
}

int main (int argc, char **argv)
{
    if (argc < 5) {
        printf("Usage: ./lr9 portion_bee bear_portion count_bee bear_time\n");
        exit(1);
    }
    int portion_bee = atoi(argv[1]); //порция, которую приносят пчелы
    int bear_portion = atoi(argv[2]); //порция, которую съедает медведь
    int count_bee = atoi(argv[3]); //количество пчел
    int bear_time = atoi(argv[4]); //время потребления порции медведем
    //проверка корректности
    if (portion_bee < 0 || bear_portion < 0 || count_bee < 0 || bear_time < 0){
        printf("Значения не могут быть отрицательными!\n");
        exit(1);
    }
    bear *shm;
    pid_t pid[count_bee];
    key_t key; //один и тотже ключ можно использовать как для семафора, так и для разделяемой памяти
    //для разделяемой памяти
    int shmid;
    //для семафора
    int semid;
    union semun arg;

    //Получение ключа
    if ((key = ftok(".", 'S')) < 0) {
        printf("Ошибка при получении ключа\n");
        exit(1);
    }
    //Создаём семафор для синхронизации работы с разделяемой памятью
    semid = semget(key, 1, 0666 | IPC_CREAT);
    //Установка в семафоре №0 значения "1"
    arg.val = 1;
    semctl (semid, 0, SETVAL, arg);

    //Создание области разделяемой памяти
    if ((shmid = shmget(key, sizeof(bear), IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    //Получение доступа к разделяемой памяти
    if ((shm = (bear*)shmat(shmid, NULL, 0)) == (bear*) -1) {
        perror("shmat");
        exit(1);
    }
    shm->barrel = 0;
    shm->time_life = bear_time;
    //Отключаемся от разделяемой памяти
    if (shmdt(shm) < 0){
        perror ("shmdt");
        exit(1);
    }
    int i;
    //Начинаем работу с процессами
    printf("ПЧЁЛЫ НАЧИНАЮТ РАБОТУ!\n");
    for (i=0; i< count_bee; i++){
        pid[i] = fork();
        srand (getpid());
        switch(pid[i])
        {
            case -1:
                perror("fork");
                exit(1);
            case 0:
                while(1)
                    child_bee (shm, shmid, semid, i, portion_bee);
            return 0;
        }
    }
    //родительский процесс
    int result = 1;
    while (1) {
        result = parent(shm, shmid, semid, bear_portion, bear_time);
        if (result == 1)
            continue;
        else {
            clear(shmid, semid, pid, count_bee);
            break;
        }
    }
    return 0;
}
