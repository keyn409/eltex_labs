#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/ethernet.h>
#include <linux/if.h>
#include <unistd.h>

//Mac-адрес клиента
#define CLIENT_MAC0 0xf0
#define CLIENT_MAC1 0xd5
#define CLIENT_MAC2 0xbf
#define CLIENT_MAC3 0xd2
#define CLIENT_MAC4 0x2b
#define CLIENT_MAC5 0x61

//Mac-адрес сервера
#define SERVER_MAC0   0xe8
#define SERVER_MAC1   0xb1
#define SERVER_MAC2   0xfc
#define SERVER_MAC3   0xd2
#define SERVER_MAC4   0xc2
#define SERVER_MAC5   0x4c

#define ETHER_TYPE  0x0800

#define DEFAULT_IF  "eth0" //Если программа запускается без параметров, то в качестве интерфейса выбирается DEFAULT_IF
#define BUF_SIZ     1024

//функция отправки клиенту сообщения
int send_to_client(char* ifName, uint8_t* buf, ssize_t numbytes) {
    int sockfd;
    struct ifreq if_idx;
    struct ifreq if_mac;
    int tx_len = 0;
    char sendbuf[BUF_SIZ];
    struct ether_header *eh = (struct ether_header *) sendbuf;
    struct sockaddr_ll socket_address;

    //Создаём raw сокет для отправки
    if ((sockfd = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1) {
        perror("socket");
        return -1;
    }

    //Получаем индекс интерфейса, с которого будем отправлять
    memset(&if_idx, 0, sizeof(struct ifreq));
    strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
    if (ioctl(sockfd, SIOCGIFINDEX, &if_idx) < 0)
        perror("SIOCGIFINDEX");
    //Получаем MAC-адрес интерфейса
    memset(&if_mac, 0, sizeof(struct ifreq));
    strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
    if (ioctl(sockfd, SIOCGIFHWADDR, &if_mac) < 0) {
        perror("SIOCGIFHWADDR");
        return -1;
    }
    //Конструируем заголовок Ethernet
    memset(sendbuf, 0, BUF_SIZ);
    //Заполняем MAC-адрес отправителя
    eh->ether_shost[0] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[0];
    eh->ether_shost[1] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[1];
    eh->ether_shost[2] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[2];
    eh->ether_shost[3] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[3];
    eh->ether_shost[4] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[4];
    eh->ether_shost[5] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[5];
    //Заполняем MAC-адрес получателя
    eh->ether_dhost[0] = CLIENT_MAC0;
    eh->ether_dhost[1] = CLIENT_MAC1;
    eh->ether_dhost[2] = CLIENT_MAC2;
    eh->ether_dhost[3] = CLIENT_MAC3;
    eh->ether_dhost[4] = CLIENT_MAC4;
    eh->ether_dhost[5] = CLIENT_MAC5;

    // Указываем EtherType
    eh->ether_type = htons(ETH_P_IP);

    //Определяем размер сообщения и копируем сообщение клиента
    tx_len += sizeof(struct ether_header);
    for ( int i=14; i<numbytes; i++)
        sendbuf[tx_len++] = (char)buf[i];

    //Указываем индекс сетевого устройства
    socket_address.sll_ifindex = if_idx.ifr_ifindex;

    //Длина адреса
    socket_address.sll_halen = ETH_ALEN;

    //Указываем MAC-адрес получателя
    socket_address.sll_addr[0] = CLIENT_MAC0;
    socket_address.sll_addr[1] = CLIENT_MAC1;
    socket_address.sll_addr[2] = CLIENT_MAC2;
    socket_address.sll_addr[3] = CLIENT_MAC3;
    socket_address.sll_addr[4] = CLIENT_MAC4;
    socket_address.sll_addr[5] = CLIENT_MAC5;

    //Отправляем пакет
    if (sendto(sockfd, sendbuf, tx_len, 0, (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed\n");
    //Закрываем сокет
    close(sockfd);
    return 0;
}

int main(int argc, char *argv[])
{
    int sockfd, i;
    int sockopt;
    ssize_t numbytes;
    struct ifreq ifopts; //устанавливаем неразборчивый режим
    uint8_t buf[BUF_SIZ];
    char ifName[IFNAMSIZ];
    //Получаем имя интерфейса от пользователя
    if (argc > 1)
        strcpy(ifName, argv[1]);
    else
        strcpy(ifName, DEFAULT_IF);
    //Заполняем структуры заголовков
    struct ether_header *eh = (struct ether_header *) buf;

    //Открываем raw сокет с типом EtherType
    if ((sockfd = socket(PF_PACKET, SOCK_RAW, htons(ETHER_TYPE))) == -1) {
        perror("listener: socket");
        return -1;
    }
    //Устанавливаем интерфейс в неразборчивый режим
    strncpy(ifopts.ifr_name, ifName, IFNAMSIZ-1);
    ioctl(sockfd, SIOCGIFFLAGS, &ifopts);
    ifopts.ifr_flags |= IFF_PROMISC;
    ioctl(sockfd, SIOCSIFFLAGS, &ifopts);
    //Разрешаем повторное использование сокета
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof sockopt) == -1) {
        perror("setsockopt");
        close(sockfd);
        return -1;
    }
    //Привязываем устройство (bind to device)
    if (setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, ifName, IFNAMSIZ-1) == -1)  {
        perror("SO_BINDTODEVICE");
        close(sockfd);
        exit(EXIT_FAILURE);
    }
    //Начинаем прослушивание
    while(1) {
        printf("Сервер: Ожидаю пакет...\n");
        numbytes = recvfrom(sockfd, buf, BUF_SIZ, 0, NULL, NULL);
        printf("Сервер: Получил пакет размер %lu байт\n", numbytes);
        //Проверяем, действительно ли пакет предназначен для сервера
        if (eh->ether_dhost[0] == SERVER_MAC0 &&
            eh->ether_dhost[1] == SERVER_MAC1 &&
            eh->ether_dhost[2] == SERVER_MAC2 &&
            eh->ether_dhost[3] == SERVER_MAC3 &&
            eh->ether_dhost[4] == SERVER_MAC4 &&
            eh->ether_dhost[5] == SERVER_MAC5) {
                printf("Верный MAC-адрес получателя\n");
        } else { //если неверный MAC-адрес сервера, ожидаем следующий пакет
            printf("Неверный MAC-адрес получателя\n");
            continue;
        }
        //Выводим содержимое пакета
        printf("\tДанные:");
        for (i=0; i<numbytes; i++)
            printf("%02x:", buf[i]);
        printf("\n\n\n");
        //Проверяем MAC-адрес. Если пакет пришёл от нужного клиента, отправляем ему ответ
        if (eh->ether_shost[0] == CLIENT_MAC0 &&
            eh->ether_shost[1] == CLIENT_MAC1 &&
            eh->ether_shost[2] == CLIENT_MAC2 &&
            eh->ether_shost[3] == CLIENT_MAC3 &&
            eh->ether_shost[4] == CLIENT_MAC4 &&
            eh->ether_shost[5] == CLIENT_MAC5)
                send_to_client(ifName, buf, numbytes);
    }
    //Закрываем сокет
    close(sockfd);
    return 0;
}
