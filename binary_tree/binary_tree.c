#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

typedef enum binary_tree_consts
{
    BIN_TREE_ELEM_MAX_VALUE = 10000000,
} binary_tree_consts;

typedef struct Node
{
    long value;
    int left_height;
    int right_height;
    struct Node *left;
    struct Node *right;
} Node;


void print_tree_values(struct Node *const node)
{
    if (NULL == node)
    {
        return;
    }

    printf("Node %p, value - %ld\n", node, node->value);
    print_tree_values(node->left);
    print_tree_values(node->right);
}

int insert(struct Node **const node_ptr, int const value)
{
    int ret = 0;
    Node *node = *node_ptr;
    if (NULL == node)
    {
        errno = 0;
        node = calloc(1, sizeof(struct Node));
        if (NULL == node)
        {
            printf("calloc() failed - %s\n", strerror(errno));
            ret = -1;
            goto finally;
        }

        node->value = value;
        node->left_height = 1;
        node->right_height = 1;
        *node_ptr = node;

        return node->left_height;
    }

    // If binary already have current element, don't insert it
    if (node->value == value)
    {
        ret = -2;
        goto finally;
    }
    else if (node->value > value)
    {
        ret = insert(&node->left, value);
        if (-1 == ret)
        {
            printf("insert(left) failed, return result = %d\n", ret);
            goto finally;
        }
        else if (-2 == ret)
        {
            goto finally; // We have the value, don't change height
        }
        node->left_height = ret + 1;
    }
    else
    {
        ret = insert(&node->right, value);
        if (-1 == ret)
        {
            printf("insert(right) failed, return result = %d\n", ret);
            goto finally;
        }
        else if (-2 == ret)
        {
            goto finally; // We have the value, don't change height
        }
        node->right_height = ret + 1;
    }

    (node->left_height > node->right_height) ? (ret = node->left_height) : (ret = node->right_height);

finally:
    return ret;
}

long height_1(struct Node *const node)
{
    long ret = 0;
    long left_size = 1;
    long right_size = 1;

    if (node == NULL)
    {
        goto finally;
    }

    if (NULL != node->left)
    {
        left_size = left_size + height_1(node->left);
    }

    if (NULL != node->right)
    {
        right_size = right_size + height_1(node->right);
    }

    if (left_size > right_size)
    {
        ret = left_size;
    }
    else
    {
        ret = right_size;
    }

finally:
    return ret;
}

long height_2(struct Node *const node)
{
    long ret = 0;

    if (node == NULL)
    {
        goto finally;
    }

    (node->left_height > node->right_height) ? (ret = node->left_height) : (ret = node->right_height); // +1 is head node

finally:
    return ret;
}

int height_test(int tree_element_count)
{
    int ret = 0;
    long tree_elem_value = 0;
    struct timeval before_test, after_test;
    long timeout_msec = 0;
    struct Node *root = NULL;

    for (int i = 0; i < tree_element_count; i++)
    {
        tree_elem_value = rand() % BIN_TREE_ELEM_MAX_VALUE + 1;
        ret = insert(&root, tree_elem_value);
        if (-1 == ret)
        {
            printf("insert(height_test) failed, return result = %d", ret);
            ret = 1;
            goto finally;
        }
    }

    gettimeofday(&before_test, NULL);
    printf("Tree height for %d elements - %ld\n", tree_element_count, height_1(root));
    gettimeofday(&after_test, NULL);

    timeout_msec = (long)((double)(after_test.tv_usec - before_test.tv_usec) / 1000 +
                            (double)(after_test.tv_sec - before_test.tv_sec) * 1000);
    printf("Working time for %d elements [1 part] - %ld\n", tree_element_count, timeout_msec);

    gettimeofday(&before_test, NULL);
    printf("Tree height for %d elements - %ld\n", tree_element_count, height_2(root));
    gettimeofday(&after_test, NULL);

    timeout_msec = (long)((double)(after_test.tv_usec - before_test.tv_usec) / 1000 +
                            (double)(after_test.tv_sec - before_test.tv_sec) * 1000);
    printf("Working time for %d elements [2 part] - %ld\n", tree_element_count, timeout_msec);

finally:
    return ret;
}


int main(void)
{
    int ret = 0;
    srand(time(NULL));
    height_test(10);
    height_test(1000);
    height_test(1000000);
    height_test(10000000);

    return ret;
}