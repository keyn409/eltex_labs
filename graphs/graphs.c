#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>
#include <time.h>
#include <sys/resource.h>

int edgesCount = 0;
typedef enum Graph_consts
{
	GRAPH_INFINITY = 1000,
    GRAPH_NODE_COUNT = 30000,
    GRAPH_NODE_MAX_RAND_VALUE = 100,
    SEC_TO_MKR_SEC = 1000000,
} Graph_consts;

typedef struct Edges
{
	int first, second, cost;
} Edges;

int node_count = GRAPH_NODE_COUNT;

int dijkstra_shortest_path(
    int adjacency_matrix[GRAPH_NODE_COUNT][GRAPH_NODE_COUNT],
    int const starting_node,
    int *node_weight)
{
    int ret = 0;
	bool used_nodes[GRAPH_NODE_COUNT] = {0};
	int min_node_index = 0;
	int min_weight = 0;
	int temp = 0;
	int i = 0;

    if (NULL == node_weight)
    {
        printf("Parameter node_weight in NULL\n");
        ret = -1;
        goto finally;
    }

	for (i = 0; i < GRAPH_NODE_COUNT; i++)
	{
		node_weight[i] = GRAPH_INFINITY;
	}

	node_weight[starting_node] = 0;

    for (int k = 0; k < GRAPH_NODE_COUNT; k++)
    {
		min_weight = GRAPH_INFINITY;
		for (i = 0; i < GRAPH_NODE_COUNT; i++)
		{
			if (   (false == used_nodes[i])
				&& (node_weight[i] < min_weight))
			{
				min_weight = node_weight[i];
				min_node_index = i;
			}
		}

        for (i = 0; i < GRAPH_NODE_COUNT; i++)
        {
            if (adjacency_matrix[min_node_index][i] > 0)
            {
                temp = min_weight + adjacency_matrix[min_node_index][i];
                if (temp < node_weight[i])
                {
                    node_weight[i] = temp;
                }
            }
        }

        used_nodes[min_node_index] = true;
    }

finally:
	return ret;
}

int Ford_Bellman(
    Edges edge[edgesCount],
    int const starting_node,
    int *node_weight)
{
    int i, j;
    int ret = 0;
    bool flag = true;

    if (NULL == node_weight)
    {
        printf("Parameter node_weight in NULL\n");
        ret = -1;
        goto finally;
    }

    for (i = 0; i < GRAPH_NODE_COUNT; i++)
    {
        node_weight[i] = GRAPH_INFINITY;
    }

    node_weight[starting_node] = 0;
    for (i = 0; i < GRAPH_NODE_COUNT-1; ++i)
    {
    	flag = true;
        for (j = 0; j < edgesCount; ++j)
        {
        	if(node_weight[edge[j].first] < GRAPH_INFINITY)
        	{
	            if (node_weight[edge[j].first] + edge[j].cost < node_weight[edge[j].second])
	            {
					node_weight[edge[j].second] = node_weight[edge[j].first] + edge[j].cost;
					flag = false;
	            }
	        }
        }
        if (flag == true)
        	break;
    }

finally:
    return ret;
}


int Ford_Bellman_test(void)
{
    int ret = 0;
    int adjacency_matrix[GRAPH_NODE_COUNT][GRAPH_NODE_COUNT] = {0};
	int node_weight[GRAPH_NODE_COUNT] = {0};
	Edges edge[GRAPH_NODE_COUNT*GRAPH_NODE_COUNT/2] = {0};
    int i = 0;
    int j = 0;
    int edge_weight = 0;
    struct timeval before_test, after_test;
    long timeout_msec = 0;

    for (i = 0; i < GRAPH_NODE_COUNT; i++)
	{
		adjacency_matrix[i][i] = 0;
		for (j = i + 1; j < GRAPH_NODE_COUNT; j++)
        {
            edge_weight = rand() % GRAPH_NODE_MAX_RAND_VALUE;
            adjacency_matrix[i][j] = edge_weight;
			adjacency_matrix[j][i] = edge_weight;

			if(edge_weight != 0)
			{
				edge[edgesCount].first = i;
				edge[edgesCount].second = j;
				edge[edgesCount].cost = edge_weight;
				edgesCount++;
			}
		}
	}

    // for (i = 0; i<node_count; i++)
    // {
    //     for (j = 0; j<node_count; j++)
    //     printf("%5d ", adjacency_matrix[i][j]);
    //     printf("\n");
    // }

    // for (i=0; i<edgesCount; i++)
    // {
    // 	printf("[%d->%d] %d\n", edge[i].first, edge[i].second, edge[i].cost);
    // }

    gettimeofday(&before_test, NULL);
    ret = Ford_Bellman(edge, 0, node_weight);
    if (0 != ret)
    {
        printf("Ford_Bellman() failed, return result = %d\n", ret);
        ret = -1;
        goto finally;
    }
    gettimeofday(&after_test, NULL);

    timeout_msec = (long)((double)(after_test.tv_usec - before_test.tv_usec) +
                            (double)(after_test.tv_sec - before_test.tv_sec) * SEC_TO_MKR_SEC);
    printf("Working time for %d elements [Ford_Bellman] - %ld\n", GRAPH_NODE_COUNT, timeout_msec);

 	// printf("Кратчайшие расстояния до вершин:\n");
	// for (i = 0; i < node_count; i++)
	// {
	// 	printf("%5d ", node_weight[i]);
	// }

    //printf("\n");
    memset (&node_weight, 0, sizeof(node_weight));
    gettimeofday(&before_test, NULL);
    ret = dijkstra_shortest_path(adjacency_matrix, 0, node_weight);
    if (0 != ret)
    {
        printf("dijkstra_shortest_path() failed, return result = %d\n", ret);
        ret = -1;
        goto finally;
    }
    gettimeofday(&after_test, NULL);

    timeout_msec = (long)((double)(after_test.tv_usec - before_test.tv_usec) +
                            (double)(after_test.tv_sec - before_test.tv_sec) * SEC_TO_MKR_SEC);
    printf("Working time for %d elements [Dijkstra] - %ld\n", GRAPH_NODE_COUNT, timeout_msec);

 	// printf("Кратчайшие расстояния до вершин:\n");
	// for (i = 0; i < node_count; i++)
	// {
	// 	printf("%5d ", node_weight[i]);
	// }

    //printf("\n");

finally:
    return ret;
}

int main()
{
    int ret = 0;
    struct rlimit x = {0};

    srand(time(NULL));

    /* Увеличиваем размер стека для процесса, так как количество элементов в массиве
       очень большое */
    errno = 0;
    if (getrlimit(RLIMIT_STACK, &x) < 0)
    {
        printf("getrlimit() failed - %s\n", strerror(errno));
        ret = -1;
        goto finally;
    }

    x.rlim_cur = RLIM_INFINITY;

    errno = 0;
    if (setrlimit(RLIMIT_STACK, &x) < 0)
    {
        printf("setrlimit() failed - %s\n", strerror(errno));
        ret = -2;
        goto finally;
    }

    ret = Ford_Bellman_test();
    if (0 != ret)
    {
        printf("Ford_Bellman_test() failed, return result = %d", ret);
        ret = -3;
        goto finally;
    }

finally:
    return ret;
}