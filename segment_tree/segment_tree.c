#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>
#include <time.h>
#include <sys/resource.h>

typedef enum Segment_tree_consts
{
	ST_RAND_MAX = 10,
	SEC_TO_MKR_SEC = 1000000,
} Segment_tree_consts;

void build_segment_tree(
	short *array,
	int *segment_tree,
	int current_node,
	int left_side,
	int right_side)
{
	if (left_side == right_side)
	{
		segment_tree[current_node] = array[left_side];
	}
	else
	{
		int middle = (left_side + right_side) / 2;
		build_segment_tree(array, segment_tree, current_node * 2,
			left_side, middle);
		build_segment_tree(array, segment_tree, current_node * 2 + 1,
			middle + 1, right_side);
		segment_tree[current_node] = segment_tree[current_node * 2] +
			segment_tree[current_node * 2 + 1];
	}
}

void build_fenvik_tree(
	int idx,
	int *fenvik_tree,
	short delta,
	int elements_count)
{
	int i = idx;

    for (; i < elements_count; i |= i + 1) 
    {
        fenvik_tree[i] += delta;
    }
}

int sum_segment_tree(
	int *segment_tree,
	int current_node,
	int left_border,
	int right_border,
	int left,
	int right)
{
	if (left > right)
	{
		return 0;
	}

	if (left == left_border
		&& right == right_border)
	{
		return segment_tree[current_node];
	}

	int middle = (left_border + right_border) / 2;
	int min = (right < middle ? right : middle);
	int max = (left > middle + 1 ? left: middle + 1);
	return sum_segment_tree(segment_tree, current_node * 2,
		left_border, middle, left, min) + sum_segment_tree(segment_tree,
			current_node * 2 + 1, middle + 1, right_border, max, right);;
}

// sum from 0 to x
int sum_fenvik_tree(int x, int *fenvik_tree) {
    int result = 0;

    for (; x >= 0; x = (x & (x + 1)) - 1) {
        result += fenvik_tree[x];
    }

    return result;
}

// sum from left to right
int sum_fenvik_tree_result(int left, int right, int *fenvik_tree) {
    if (left) {
        return sum_fenvik_tree(right, fenvik_tree) - sum_fenvik_tree(left - 1, fenvik_tree);
    } else {
        return sum_fenvik_tree(right, fenvik_tree);
    }
}

int segment_tree_test(int elements_count)
{
	int ret = 0;
	int i = 0;
	short *array = NULL;
	int *segment_tree = NULL;
	int *fenvik_tree = NULL;
	struct timeval before_test, after_test;
    long timeout_msec = 0;
	int left = 0;
	int right = 0;

	errno = 0;
	array = calloc(elements_count, sizeof(short));
	if (NULL == array)
	{
		printf("calloc() failed - %s\n", strerror(errno));
		ret = -1;
		goto finally;
	}

	segment_tree = calloc(elements_count * 4, sizeof(int));
	if (NULL == segment_tree)
	{
		printf("calloc() failed - %s\n", strerror(errno));
		ret = -2;
		goto finally;
	}

	fenvik_tree = calloc(elements_count, sizeof(int));
	if (NULL == fenvik_tree)
	{
		printf("calloc() failed - %s\n", strerror(errno));
		ret = -2;
		goto finally;
	}

	for (i = 0; i < elements_count; i++)
	{
		array[i] = rand() % ST_RAND_MAX;
		if (array[i] < 0)
		{
			printf("array[i] < 0!!! - %d\n", array[i]);
		}
		build_fenvik_tree(i, fenvik_tree, array[i], elements_count);
	}

	left = rand() % (elements_count/2 - 1);
	right = elements_count/2 + rand() % elements_count / 2;

	// for (int i = 0; i < elements_count; i++)
	// {
	// 	printf("%5d", array[i]);
	// }
	// printf("\n");

	build_segment_tree(array, segment_tree, 1, 0, elements_count - 1);

	// for (int i = 0; i < 4 * elements_count; i++)
	// {
	// 	printf("%5d", segment_tree[i]);
	// }
	// printf("\n");

	gettimeofday(&before_test, NULL);
	printf("Sum from %d to %d (array size - %d) [segment tree]: %d\n", left, right, elements_count,
		sum_segment_tree(segment_tree, 1, 0, elements_count-1, left, right));
    gettimeofday(&after_test, NULL);

    timeout_msec = (long)((double)(after_test.tv_usec - before_test.tv_usec) +
                            (double)(after_test.tv_sec - before_test.tv_sec) * SEC_TO_MKR_SEC);
    printf("Working time of segment tree - %ld\n", timeout_msec);

    gettimeofday(&before_test, NULL);
    printf("Sum from %d to %d (array size - %d) [fenvik tree]: %d\n", left, right, elements_count,
    	sum_fenvik_tree_result(left, right, fenvik_tree));
    gettimeofday(&after_test, NULL);

    timeout_msec = (long)((double)(after_test.tv_usec - before_test.tv_usec) +
                            (double)(after_test.tv_sec - before_test.tv_sec) * SEC_TO_MKR_SEC);
    printf("Working time of fenvik tree - %ld\n", timeout_msec);

finally:
	free(array);
	free(segment_tree);
	free(fenvik_tree);
	return ret;
}

int main(void)
{
	int ret = 0;
	srand(time(NULL));

	segment_tree_test(10);
	segment_tree_test(1000);
	segment_tree_test(1000000);
	segment_tree_test(10000000);

    return ret;
}